package pm;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import pm.controllers.*;
import pm.models.*;
import pm.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

class ControllerTest {

	private static JavalinApp app = new JavalinApp();

	@BeforeAll
	static void init() {
		app.start(7010);
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}

	@Test
	void postCiclista200() {
		MeioDePagamento meio = new MeioDePagamento("Romany Moore", "54321", "05-07-3041", "321");
		Ciclista ciclista = new Ciclista();

		ciclista.setNome("Bryony Gardiner");
		ciclista.setNascimento("02-08-1998");
		ciclista.setCpf("10100101000");
		ciclista.setPassaporte(new Ciclista.Passaporte());
		ciclista.getPassaporte().setNumero("123456789");
		ciclista.getPassaporte().setPais("Brasil");
		ciclista.getPassaporte().setValidade("01-01-2050");
		ciclista.setNacionalidade("brasileira");
		ciclista.setEmail("bryony@hotmail.com");
		ciclista.setSenha("psswrd1");
		ciclista.setMeioDePagamento(meio);

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista").body(ciclista).asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postCiclista405() {
		MeioDePagamento meio = new MeioDePagamento("000", "12345", "01-01-2030", "123");
		Ciclista ciclista = new Ciclista();

		ciclista.setNome("Helin Todd");
		ciclista.setNascimento("04-07-2008");
		ciclista.setCpf("2026055521");
		ciclista.setPassaporte(new Ciclista.Passaporte());
		ciclista.getPassaporte().setNumero("5555544440");
		ciclista.getPassaporte().setPais("Brasil");
		ciclista.getPassaporte().setValidade("21-05-2043");
		ciclista.setNacionalidade("brasileira");
		ciclista.setEmail("email");
		ciclista.setSenha("senha1234967");
		ciclista.setMeioDePagamento(meio);

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista").body(ciclista).asJson();
		assertEquals(200, response.getStatus());
	}
	
	@Test
	void postCiclistaAtivar200() {
		Ciclista ciclista = ControllerCiclista.mockGetCiclista();

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista/" + ciclista.getId() + "/ativar").body(ciclista).asJson();
		assertEquals(200, response.getStatus());
	}
	
	@Test
	void postCiclistaAtivar404() {
		Ciclista ciclista = ControllerCiclista.mockGetCiclista404();

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista/" + ciclista.getId() + "/ativar").body(ciclista).asJson();
		assertEquals(404, response.getStatus());
	}
	
	@Test
	void postCiclistaAtivar405() {
		MeioDePagamento meio = new MeioDePagamento("000", "12345", "01-01-2030", "123");
		Ciclista ciclista = new Ciclista();

		ciclista.setNome("Helin Todd");
		ciclista.setNascimento("04-07-2008");
		ciclista.setCpf("2026055521");
		ciclista.setPassaporte(new Ciclista.Passaporte());
		ciclista.getPassaporte().setNumero("5555544440");
		ciclista.getPassaporte().setPais("Brasil");
		ciclista.getPassaporte().setValidade("21-05-2043");
		ciclista.setNacionalidade("brasileira");
		ciclista.setEmail("email");
		ciclista.setSenha("senha1234967");
		ciclista.setMeioDePagamento(meio);

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista/" + ciclista.getId() + "/ativar").body(ciclista).asJson();
		assertEquals(405, response.getStatus());
	}

	@Test
	void getCiclistaById200() {
		String id = (ControllerCiclista.mockGetCiclista().getId());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/" + id).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getCiclistaById404() {
		String id = (ControllerCiclista.mockGetCiclista404().getId());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/" + id).asJson();
		
		assertEquals(404, response.getStatus());
	}

	@Test
	void getCiclistaById422() {
		String id = (ControllerCiclista.mockGetCiclista422().getId());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/" + id).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void putCiclistaById200() {
		Ciclista test = ControllerCiclista.mockGetCiclista();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/ciclista/" + test.getId())
				.body("{\"nome\":\"Bryony Gardiner\"}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test
	void putCiclistaById404() {
		Ciclista test = ControllerCiclista.mockGetCiclista404();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/ciclista/" + test.getId())
				.body("{\"nome\":\"Romany Moore\"}").asString();

		assertEquals(404, response.getStatus());
	}

	@Test 
	void putCiclistaById405() {
		String id = UUID.randomUUID().toString();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/ciclista/" + id)
				.body("{\"id\":\"\"}").asString();
		assertEquals(405, response.getStatus());
	}

	@Test
	void getEmail200() {
		String email = (ControllerCiclista.mockGetEmail().getEmail());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/" + email).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getEmail400() {
		String email = ControllerCiclista.mockGetEmail400().getEmail();
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/" + email).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getEmail422() {
		String email = "email";
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/" + email).asJson();

		assertEquals(422, response.getStatus());
	}

	@Test
	void getFuncionario200() {
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/").asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void postFuncionario200() {
		Funcionario funcionario = new Funcionario("12345", "psswrd", "leandro.f@email.com", "Leandro Ferreira", 18, "Gerente", "11111111");

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/funcionario").body(funcionario).asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postFuncionario422() {
		Funcionario funcionario = new Funcionario("80917", "psswrd1", "mail", "Luciano Moreira", 32, "Ajudante", "12457878787");

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/funcionario").body(funcionario).asJson();
		assertEquals(422, response.getStatus());
	}

	@Test
	void getFuncionarioById200() {
		String id = (ControllerFuncionario.mockGetFuncionario().getMatricula());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/" + id).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getFuncionarioById404() {
		String id = (ControllerFuncionario.mockGetFuncionario404().getMatricula());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/" + id).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getFuncionarioById422() {
		String id = "id";
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/" + id).asJson();

		assertEquals(422, response.getStatus());
	}

	@Test
	void putFuncionarioById200() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/funcionario/" + test.getMatricula())
				.body("{\"nome\":\"Lucas Andrade\"}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test 
	void putFuncionarioById404() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario404();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/funcionario/" + test.getMatricula())
				.body("{\"nome\":\"João Amaral\"}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test
	void putFuncionarioById422() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario422();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/funcionario/" + test.getMatricula())
				.body("{\"nome\":Gerente}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test
	void deleteFuncionario200() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario();
		HttpResponse<String> response = Unirest.delete("http://localhost:7010/funcionario/" + test.getMatricula())
				.asString();
		assertEquals(200, response.getStatus());
	}

	@Test // ************** erro
	void deleteFuncionario404() {
		HttpResponse<String> response = Unirest.delete("http://localhost:7010/funcionario/" + UUID.randomUUID())
				.asString();
		assertEquals(404, response.getStatus());
	}

	@Test
	void deleteFuncionario422() {
		String test = "qqqq";
		HttpResponse<String> response = Unirest.delete("http://localhost:7010/funcionario/" + test).asString();
		assertEquals(404, response.getStatus());
	}

	@Test
	void getCartaoById200() {
		String ciclistaId = ControllerCiclista.mockGetCiclista().getId();
		String id = UUID.randomUUID().toString();
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/" + ciclistaId).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getCartaoById404() {
		String id = UUID.randomUUID().toString();
		String ciclistaId = ControllerCiclista.mockGetCiclista404().getId();
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/" + ciclistaId).asJson();

		assertEquals(404, response.getStatus());
	}

	@Test
	void getCartaoById422() {
		String id = "7899999";
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/" + id).asJson();

		assertEquals(422, response.getStatus());
	}

	@Test
	void putCartaoById200() {
		String ciclistaId = ControllerCiclista.mockGetCiclista().getId();
		
		HttpResponse<String> response = Unirest
				.put("http://localhost:7010/cartaoDeCredito/" + ciclistaId)
				.body("{\"numero\":\"1234567890\"}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test
	void putCartaoById404() {
		String ciclistaId = ControllerCiclista.mockGetCiclista().getId();
		HttpResponse<String> response = Unirest
				.put("http://localhost:7010/cartaoDeCredito/" + UUID.randomUUID().toString())
				.body("{\"numero\":\"987456321\"}").asString();
		assertEquals(422, response.getStatus());
	}

	@Test
	void putCartaoById422() {
		String test = "eee";
		HttpResponse<String> response = Unirest.put("http://localhost:7010/cartaoDeCredito/" + test)
				.body("{\"numero\":\"12347777\"}").asString();
		assertEquals(422, response.getStatus());
	}

	@Test
	void postAluguel200() {
		ControllerCiclista.mockGetCiclista();
		Aluguel aluguel = new Aluguel(UUID.randomUUID().toString(), UUID.randomUUID().toString());

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/aluguel").body(aluguel).asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postAluguel422() {
		ControllerCiclista.mockGetCiclista404();
		Aluguel aluguel = new Aluguel(null, UUID.randomUUID().toString());

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/aluguel").body(aluguel).asJson();
		assertEquals(422, response.getStatus());
	}

	@Test
	void postDevolucao200() {
		Devolucao devolucao = new Devolucao(UUID.randomUUID().toString(), UUID.randomUUID().toString());

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/devolucao").body(devolucao).asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postDevolucao422() {
		Devolucao devolucao = ControllerDevolucao.mockGetDevolucao422();

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/devolucao").body(devolucao).asJson();
		assertEquals(422, response.getStatus());
	}

}
