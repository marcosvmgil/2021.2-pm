package pm.models;

public class Cobranca {
	public static float valor;
	public static String ciclista;
	/* atributos erro */

	public Cobranca(float valor, String ciclista) {
		this.valor = valor;
		this.ciclista = ciclista;
	}

	Cobranca() {

	}

	public static String getCiclista() {
		return ciclista;
	}

	public static void setCiclista(String ciclista) {
		Cobranca.ciclista = ciclista;
	}

	public static float getValor() {
		return valor;
	}

	public static void setValor(float valor) {
		Cobranca.valor = valor;
	}

}
