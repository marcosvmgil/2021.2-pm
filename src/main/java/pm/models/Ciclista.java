package pm.models;

import java.util.UUID;

public class Ciclista {
	private String id;
	private String nome;
	private String nascimento;
	private String cpf;
	private String nacionalidade;
	private String email;
	private String senha;
	private Passaporte passaporte;
	private MeioDePagamento meioDePagamento;

	
	public Ciclista() {
		generateId();
	}
	
	public void generateId() {
		if (id == null) {
			UUID uuid = UUID.randomUUID();
			this.id = uuid.toString();
		}
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNascimento() {
		return nascimento;
	}

	public void setNascimento(String nascimento) {
		this.nascimento = nascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Passaporte getPassaporte() {
		return passaporte;
	}

	public void setPassaporte(Passaporte passaporte) {
		this.passaporte = passaporte;
	}

	public MeioDePagamento getMeioDePagamento() {
		return meioDePagamento;
	}

	public void setMeioDePagamento(MeioDePagamento meioDePagamento) {
		this.meioDePagamento = meioDePagamento;
	}

	@Override
	public String toString() {
		return "Ciclista [idCiclista=" + id + ", nome=" + nome + ", nascimento=" + nascimento + ", cpf=" + cpf
				+ ", nacionalidade=" + nacionalidade + ", email=" + email + ", senha=" + senha + ", passaporte="
				+ passaporte + ", meioDePagamento=" + meioDePagamento + "]";
	}

	public static class Passaporte {
		private String numero;
		private String validade;
		private String pais;

		@Override
		public String toString() {
			return "Passaporte [numero=" + numero + ", validade=" + validade + ", pais=" + pais + "]";
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		public String getValidade() {
			return validade;
		}

		public void setValidade(String validade) {
			this.validade = validade;
		}

		public String getPais() {
			return pais;
		}

		public void setPais(String pais) {
			this.pais = pais;
		}
	}

}
