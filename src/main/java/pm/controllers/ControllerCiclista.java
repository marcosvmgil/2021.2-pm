package pm.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

import io.javalin.http.Context;
import pm.models.Ciclista;
import pm.models.Ciclista.Passaporte;
import pm.models.MeioDePagamento;

public class ControllerCiclista {
	protected static List<Ciclista> ciclistas = new ArrayList<>();
	private static final Pattern email = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	
	ControllerCiclista(){}
	
	private static String dadosCadastrados = "200 - Dados cadastrados";
	private static String dadosNaoEncontados = "404 - Dados não encontrados";
	public static void postCiclista(Context ctx) {
		Gson gson = new Gson();
		Ciclista obterCiclista = gson.fromJson(ctx.body(), Ciclista.class);
		
		if(obterCiclista != null)
			obterCiclista.setId(UUID.randomUUID().toString());

		ciclistas.add(obterCiclista);
		ctx.json(new CiclistaNovo(obterCiclista));

	}

	public static void getCiclistaById(Context ctx) {
		String id = ctx.pathParam("id");
//		String body = ctx.body();
		Ciclista ciclista = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

		if (ciclista != null) {
			Ciclista aux = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

			if (aux != null) {
				ctx.status(200);
				ctx.result(dadosCadastrados);
			} else {
				ctx.status(422);
				ctx.result("422 - Dados inválidos");
			}
		} else {
			ctx.status(404);
			ctx.result(dadosNaoEncontados);
		}

	}

	public static void putCiclistaById(Context ctx) {
		String id = ctx.pathParam("id");
		String body = ctx.body();
		Ciclista ciclista = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

		if (ciclista != null) {
			Ciclista aux = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

			if (aux != null) {
				Gson gson = new Gson();
				Ciclista novoCiclista = gson.fromJson(body, Ciclista.class);
				
				ciclista.setCpf(novoCiclista.getCpf());
				ciclista.setNome(novoCiclista.getNome());
				ciclista.setNascimento(novoCiclista.getNascimento());
				ciclista.setPassaporte(novoCiclista.getPassaporte());
				ciclista.setNacionalidade(novoCiclista.getNacionalidade());
				ciclista.setEmail(novoCiclista.getEmail());
				
				ctx.status(200);
				ctx.result(dadosCadastrados);
			} else {
				ctx.status(404);
				ctx.result(dadosNaoEncontados);
			}
		} else {
			ctx.status(405);
			ctx.result("405 - Dados inválidos");
		}

	}

	public static void postCiclistaAtivar(Context ctx) {
		String id = ctx.pathParam("id");
		Ciclista ciclista = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

		if (ciclista != null) {
			Ciclista aux = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

			if (aux != null) {
				ctx.status(200);
				ctx.result(dadosCadastrados);
			} else {
				ctx.status(404);
				ctx.result(dadosNaoEncontados);
			}
		} else {
			ctx.status(405);
			ctx.result("405 - Dados inválidos");
		}

	}

	public static void getCiclistaEmail(Context ctx) {
		String mail = ctx.pathParam("email");
		Ciclista ciclista = ciclistas.stream().filter(c -> mail.equals(c.getEmail())).findAny().orElse(null);

		if (ciclista != null) {

			if (email.matcher(mail).matches()) {
				ctx.status(400);
				ctx.result("400 - Email não enviado como parâmetro");
			}

			Ciclista aux = ciclistas.stream().filter(e -> mail.equals(e.getEmail())).findAny().orElse(null);

			if (aux != null) {
				ctx.status(200);
				ctx.result("200 - true");
			} else {
				ctx.status(200);
				ctx.result("200 - false");
			}
		} else {
			ctx.status(422);
			ctx.result("422 - Dados inválidos");
		}

	}

	public static Ciclista mockGetCiclista() {
		ciclistas.clear();
		MeioDePagamento meiopagamento = new MeioDePagamento("Bryony Gardiner", "12345", "25-08-2030", "123");
		Ciclista ciclista1 = new Ciclista();

		ciclista1.setNome("Bryony Gardiner");
		ciclista1.setNascimento("02-08-1998");
		ciclista1.setCpf("10100101000");
		ciclista1.setPassaporte(new Ciclista.Passaporte());
		ciclista1.getPassaporte().setNumero("123456789");
		ciclista1.getPassaporte().setPais("Brasil");
		ciclista1.getPassaporte().setValidade("01-01-2050");
		ciclista1.setNacionalidade("brasileira");
		ciclista1.setEmail("bryony@hotmail.com");
		ciclista1.setSenha("psswrd1");
		ciclista1.setMeioDePagamento(meiopagamento);

		ciclistas.add(ciclista1);

		return ciclistas.get(ciclistas.size() - 1);

	}

	public static Ciclista mockGetCiclista404() {
		ciclistas.clear();
		MeioDePagamento meiopagamento2 = new MeioDePagamento("Romany Moore", "54321", "05-07-3041", "321");
		Ciclista ciclista2 = new Ciclista();

		ciclista2.setNome("Romany Moore");
		ciclista2.setNascimento("22-07-2009");
		ciclista2.setCpf("12312312300");
		ciclista2.setPassaporte(new Ciclista.Passaporte());
		ciclista2.getPassaporte().setNumero("9994511336");
		ciclista2.getPassaporte().setPais("Alemanha");
		ciclista2.getPassaporte().setValidade("19-12-2039");
		ciclista2.setNacionalidade("alemã");
		ciclista2.setEmail("romanym@hotmail.com");
		ciclista2.setSenha("psswrd2");
		ciclista2.setMeioDePagamento(meiopagamento2);
		ciclista2.setId("");
		
		ciclistas.add(ciclista2);

		return ciclistas.get(ciclistas.size() - 1);

	}

//	public static Ciclista mockGetCiclista405() {
//		ciclistas.clear();
//		MeioDePagamento meiopagamento3 = new MeioDePagamento("Helin Todd", "22333", "31-07-2043", "223");
//		Ciclista ciclista3 = new Ciclista();
//
//		ciclista3.setNome("Helin Todd");
//		ciclista3.setNascimento("18-03-2011");
//		ciclista3.setCpf("12233344441");
//		ciclista3.setPassaporte(new Ciclista.Passaporte());
//		ciclista3.getPassaporte().setNumero("5555544440");
//		ciclista3.getPassaporte().setPais("Brasil");
//		ciclista3.getPassaporte().setValidade("21-05-2043");
//		ciclista3.setNacionalidade("brasileira");
//		ciclista3.setEmail("helin.t@hotmail.com");
//		ciclista3.setSenha("psswrd3");
//		ciclista3.setMeioDePagamento(meiopagamento3);
//		ciclista3.setId("");
//
//		ciclistas.add(ciclista3);
//
//		return ciclistas.get(ciclistas.size() - 1);
//
//	}

	public static Ciclista mockGetCiclista422() {
		ciclistas.clear();
		MeioDePagamento meiopagamento4 = new MeioDePagamento("Ailish Watts", "55555", "05-05-1999", "111");
		Ciclista ciclista4 = new Ciclista();

		ciclista4.setNome("Ailish Watts");
		ciclista4.setNascimento("21-11-1969");
		ciclista4.setCpf("777777777777");
		ciclista4.setPassaporte(new Ciclista.Passaporte());
		ciclista4.getPassaporte().setNumero("889663973");
		ciclista4.getPassaporte().setPais("Irlanda");
		ciclista4.getPassaporte().setValidade("12-05-2025");
		ciclista4.setNacionalidade("irlandes");
		ciclista4.setEmail("ailish.s@hotmail.com");
		ciclista4.setSenha("psswrd4");
		ciclista4.setMeioDePagamento(meiopagamento4);
		ciclista4.setId("idQualquer");

		ciclistas.add(ciclista4);

		return ciclistas.get(ciclistas.size() - 1);

	}

	public static Ciclista mockGetEmail() {
		ciclistas.clear();
		MeioDePagamento meioPagamento5 = new MeioDePagamento("Selena Kelley", "11111", "20-01-2002", "333");
		Ciclista ciclista5 = new Ciclista();

		ciclista5.setNome("Selena Kelley");
		ciclista5.setNascimento("02-11-1972");
		ciclista5.setCpf("77777888888");
		ciclista5.setPassaporte(new Ciclista.Passaporte());
		ciclista5.getPassaporte().setNumero("22222000");
		ciclista5.getPassaporte().setPais("Colombia");
		ciclista5.getPassaporte().setValidade("19-05-2625");
		ciclista5.setNacionalidade("colombiana");
		ciclista5.setEmail("s.kelley@hotmail.com");
		ciclista5.setSenha("psswrd5");
		ciclista5.setMeioDePagamento(meioPagamento5);

		ciclistas.add(ciclista5);

		return ciclistas.get(ciclistas.size() - 1);

	}

	public static Ciclista mockGetEmail400() {
		ciclistas.clear();
		MeioDePagamento meioPagamento6 = new MeioDePagamento("Peter Schofield", "12345", "01-01-2056", "123");
		Ciclista ciclista6 = new Ciclista();

		ciclista6.setNome("Peter Schofield");
		ciclista6.setNascimento("28-02-1992");
		ciclista6.setCpf("7070707070");
		ciclista6.setPassaporte(new Ciclista.Passaporte());
		ciclista6.getPassaporte().setNumero("22114433779");
		ciclista6.getPassaporte().setPais("Turquia");
		ciclista6.getPassaporte().setValidade("20-10-2031");
		ciclista6.setNacionalidade("turco");
		ciclista6.setEmail("1");
		ciclista6.setSenha("juuuhhh");
		ciclista6.setMeioDePagamento(meioPagamento6);

		ciclistas.add(ciclista6);

		return ciclistas.get(ciclistas.size() - 1);

	}


	public static class PostCiclista {
		@JsonProperty
		CiclistaNovo ciclista;
		@JsonProperty
		MeioDePagamentoNovo meioDePagamento;
	}

	public static class CiclistaNovo {
		@JsonProperty
		public String id;
		public String nome;
		public String nascimento;
		public String cpf;
		public String nacionalidade;
		public String email;
		public String senha;
		@JsonProperty
		public PassaporteNovo passaporte;

		public CiclistaNovo() {

		}

		public CiclistaNovo(Ciclista ciclista) {
			if (ciclista != null) {
				this.id = ciclista.getId();
			}
			this.nome = ciclista.getNome();
			this.nascimento = ciclista.getNascimento();
			this.cpf = ciclista.getCpf();
			this.nacionalidade = ciclista.getNacionalidade();
			this.email = ciclista.getEmail();
			this.senha = ciclista.getSenha();
			if (ciclista.getPassaporte() != null) {
				this.passaporte = new PassaporteNovo(ciclista.getPassaporte());
			}
		}

		public static class PassaporteNovo {
			@JsonProperty
			public String numero;
			public String validade;
			public String pais;

			public PassaporteNovo() {

			}

			public PassaporteNovo(Passaporte passaporte) {
				this.numero = passaporte.getNumero();
				this.validade = passaporte.getValidade();
				this.pais = passaporte.getPais();
			}

			public Passaporte obterPassaporte() {
				Passaporte passaporte = new Passaporte();

				String validade = this.validade;

				passaporte.setNumero(this.numero);
				passaporte.setValidade(validade);
				passaporte.setPais(this.pais);

				return passaporte;
			}
		}

		public Ciclista obterCiclista() {
			Ciclista ciclista = new Ciclista();

			String nascimento = this.nascimento;

			if (this.id != null) {
				ciclista.setId((this.id));
			}
			ciclista.setNome(this.nome);
			ciclista.setNascimento(nascimento);
			ciclista.setCpf(this.cpf);
			ciclista.setNacionalidade(this.nacionalidade);
			ciclista.setEmail(this.email);
			ciclista.setSenha(this.senha);
			if (this.passaporte != null) {
				ciclista.setPassaporte(this.passaporte.obterPassaporte());
			}

			return ciclista;
		}
	}

	public static class MeioDePagamentoNovo {
		@JsonProperty
		String nomeTitular;
		String numero;
		String validade;
		String cvv;

		public String getNomeTitular() {
			return nomeTitular;
		}

		public void setNomeTitular(String nomeTitular) {
			this.nomeTitular = nomeTitular;
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		public String getValidade() {
			return validade;
		}

		public void setValidade(String validade) {
			this.validade = validade;
		}

		public String getCvv() {
			return cvv;
		}

		public void setCvv(String cvv) {
			this.cvv = cvv;
		}
	}


}
