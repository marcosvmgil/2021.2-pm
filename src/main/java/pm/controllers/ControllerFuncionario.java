package pm.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import io.javalin.http.Context;
import pm.models.Funcionario;

public class ControllerFuncionario {
	
	private static List<Funcionario> funcionarios = new ArrayList<>();
	public static final Pattern email = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	private static String dadosInvalidos = "422 - Dados inválidos";
	private static String dadosNaoEncontrados = "404 - Dados não encontrados";
	
	ControllerFuncionario(){}


	public static void getFuncionario(Context ctx) {
		String result = funcionarios.toString();
		ctx.result(result);
		ctx.status(200);
	}

	public static void postFuncionario(Context ctx) {
		String body = ctx.body();
		Gson gson = new Gson();
		Funcionario funcionario = gson.fromJson(body, Funcionario.class);
		Funcionario funcionarioNovo;
		
		if (email.matcher(funcionario.getEmail()).matches()) {
			funcionarioNovo = new Funcionario(funcionario.getMatricula(), funcionario.getSenha(),
					funcionario.getEmail(), funcionario.getNome(), funcionario.getIdade(), funcionario.getFuncao(),
					funcionario.getCpf());

			funcionarios.add(funcionarioNovo);
		} else {
			funcionarioNovo = null;
		}
		

		if (funcionarioNovo != null) {
			ctx.status(200);
			funcionarios.add(funcionarioNovo);
			ctx.result("200 - Dados cadastrados - id: " + funcionarioNovo.getId());
		} else {
			ctx.status(422);
			ctx.result(dadosInvalidos);
		}

	}

	public static void getFuncionarioById(Context ctx) {
		String id = ctx.pathParam("id");
		Funcionario funcionario = null;
		if (id != null) {
			funcionario = funcionarios.stream().filter(funcionarios -> id.equals(funcionarios.getMatricula())).findAny()
					.orElse(null);
		}

		if (funcionario != null) {
			Funcionario temp = funcionarios.stream().filter(f -> id.equals(f.getMatricula())).findAny().orElse(null);

			if (temp != null) {
				ctx.status(200);
				ctx.result("200 - Dados cadastrados");
			} else {
				ctx.status(404);
				ctx.result(dadosNaoEncontrados);
			}
		} else {
			ctx.status(422);
			ctx.result(dadosInvalidos);
		}
	}

	public static void putFuncionarioById(Context ctx) {
		String id = ctx.pathParam("id");
		String body = ctx.body();
		Funcionario funcionario = null;
		if (id != null) {
			funcionario = funcionarios.stream().filter(funcionarios -> id.equals(funcionarios.getMatricula())).findAny()
					.orElse(null);
		}

		if (funcionario != null) {
			Funcionario aux = funcionarios.stream().filter(f -> id.equals(f.getMatricula())).findAny().orElse(null);

			if (aux != null) {
				if ((id != null) && (funcionario != null)) {
					Gson gson = new Gson();
					Funcionario funcionarioNovo = gson.fromJson(body, Funcionario.class);

					funcionario.setSenha(funcionarioNovo.getSenha());
					funcionario.setEmail(funcionarioNovo.getEmail());
					funcionario.setNome(funcionarioNovo.getNome());
				}
				ctx.status(200);
				ctx.result("200 - Dados cadastrados");
			} else {
				ctx.status(404);
				ctx.result(dadosNaoEncontrados);
			}
		} else {
			ctx.status(422);
			ctx.result(dadosInvalidos);
		}
	}

	public static void deleteFuncionarioById(Context ctx) {
		String id = ctx.pathParam("id");
		if (id != null) {
			Funcionario funcionario = null;
			if (id != null) {
				funcionario = funcionarios.stream().filter(funcionarios -> id.equals(funcionarios.getMatricula())).findAny()
						.orElse(null);
			}
			
			if (funcionario != null) {

				funcionarios.removeIf(f -> f.getMatricula().equals(id));
				ctx.status(200);
				ctx.result("200 - Dados removidos");
			} else {
				ctx.status(404);
				ctx.result(dadosNaoEncontrados);
			}
		} else {
			ctx.status(422);
			ctx.result(dadosInvalidos);
		}

	}

	public static Funcionario mockGetFuncionario() {
		funcionarios.clear();
		String id = UUID.randomUUID().toString();
		Funcionario funcionario = new Funcionario(id, "psswrd", "leandro.f@email.com", "Leandro Ferreira", 18, "Gerente", "11111111");
		funcionarios.add(funcionario);
		funcionarios.add(funcionario);

		return funcionarios.get(funcionarios.size() - 1);

	}

	public static Funcionario mockGetFuncionario404() {
		funcionarios.clear();
		String id2 = UUID.randomUUID().toString();
		Funcionario funcionario2 = new Funcionario(id2, "psswrd2", "gabriela.s@email.com", "Gabriela Santiago", 25, "Administradora",
				"124567891230");
		funcionarios.add(funcionario2);

		return funcionarios.get(funcionarios.size() - 1);

	}

	public static Funcionario mockGetFuncionario422() {
		funcionarios.clear();
		String id = "hehe";
		Funcionario funcionario3 = new Funcionario(id, "psswrd3", "m", "Thiago Lira", 18, "Gerente", "333333333");
		funcionarios.add(funcionario3);

		return funcionarios.get(funcionarios.size() - 1);

	}

}
