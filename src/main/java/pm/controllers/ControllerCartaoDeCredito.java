package pm.controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import io.javalin.http.Context;
import pm.models.Ciclista;
import pm.models.MeioDePagamento;

public class ControllerCartaoDeCredito {
	static List<MeioDePagamento> cartoes = new ArrayList<>();
	
	private ControllerCartaoDeCredito() {}

	public static void getCartaoById(Context ctx) {
		String id = ctx.pathParam("id");
		Ciclista ciclista = ControllerCiclista.ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

		if (ciclista != null) {
			MeioDePagamento meio = ciclista.getMeioDePagamento();

			if (meio != null) {
				ctx.status(200);
				ctx.result(meio.toString());
			} else {
				ctx.status(404);
				ctx.result("404 - Dados cadastrados");
			}
		} else {
			ctx.status(422);
			ctx.result("422 - Dados inválidos");
		}
	}

	public static void putCartaoById(Context ctx) {
		String body = ctx.body();
		String id = ctx.pathParam("id");
		Ciclista ciclista = ControllerCiclista.ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);
		
		if (ciclista != null)  {
			MeioDePagamento meio = ciclista.getMeioDePagamento();
			
			if ((ciclista.getId() != null) && (meio != null)) {
				Gson gson = new Gson();
				MeioDePagamento cartaoNovo = gson.fromJson(body, MeioDePagamento.class);

				meio.setNomeTitular(cartaoNovo.getNomeTitular());
				meio.setNumero(cartaoNovo.getNumero());
				meio.setValidade(cartaoNovo.getValidade());
				meio.setCvv(cartaoNovo.getCvv());

			}

			if (meio != null) {
				ctx.status(200);
				ctx.result("200 - Dados cadastrados");
			} else {
				ctx.status(404);
				ctx.result("404 - Dados não encontrados");
			}
		} else {
			ctx.status(422);
			ctx.result("422 - Dados inválidos");
		}

	}

}
