package pm.controllers;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.google.gson.Gson;

import io.javalin.http.Context;
import pm.models.Aluguel;
import pm.models.Cobranca;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import java.util.UUID;


public class ControllerAluguel {
	static List<Aluguel> alugueis = new ArrayList<>();
	private static Logger log = Logger.getLogger("ControllerAluguel");

	private ControllerAluguel() {}
	
	public static void postAluguel(Context ctx) {
		String body = ctx.body();
		Gson gson = new Gson();
		Aluguel aluguel = gson.fromJson(body, Aluguel.class);
		Aluguel aluguelNovo = null;
		
		if (aluguel.getCiclista() != null) {
			aluguelNovo = new Aluguel(aluguel.getCiclista(), aluguel.getTrancaInicio());
		}

		if (aluguelNovo != null) {
			ctx.status(200);
			alugueis.add(aluguelNovo);
			ctx.result("200 - Dados cadastrados");
			realizaCobranca();
			ControllerDevolucao.alterarEstadoTranca("DESTRANCAR");
			enviaEmail();

		} else {
			ctx.status(422);
			ctx.result("422 - Dados inválidos");
		}

	}

	public static void realizaCobranca() {
		String idCiclista = ControllerCiclista.ciclistas.get(0).getId();

		HttpResponse<JsonNode> response = Unirest.post("https://pmg4.herokuapp.com/cobranca/")
												.header("Content-Type", "application/json")
												.body(new Cobranca(2.0f, idCiclista))
												.asJson();
		if (response.getStatus() == 200) {
			log.info("Cobrança feita com sucesso");
		} else {
			log.debug("Erro ao fazer a cobrança");
		}
	}

	public static void enviaEmail() {
		String email = ControllerCiclista.ciclistas.get(0).getEmail();
		Email novo = new Email(email, "Requisição recebida");
		HttpResponse<JsonNode> response = Unirest.post("https://pmg4.herokuapp.com/enviarEmail")
												.header("Content-Type", "application/json")
												.body(novo)
												.asJson();
		
		if (response.getStatus() == 200) {
			log.debug("Email enviado com sucesso");
		} else {
			log.debug("Erro ao enviar email");
		}
	}

	public static class Email {
		String email;
		String mensagem;

		Email() {

		}

		public Email(String emailEnd, String mensagemCont) {
			this.email = emailEnd;
			this.mensagem = mensagemCont;
		}
	}
}
