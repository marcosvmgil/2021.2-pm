package pm.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.javalin.http.Context;
import pm.models.Cobranca;
import pm.models.Devolucao;
import com.google.gson.Gson;

import org.apache.log4j.Logger;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;


public class ControllerDevolucao {
	static List<Devolucao> devolucoes = new ArrayList<>();
	private static Logger log = Logger.getLogger("ControllerDevolucao");
	
	private ControllerDevolucao() {}

	public static void postDevolucao(Context ctx) {
		String body = ctx.body();
		Gson gson = new Gson();
		Devolucao devolucao = gson.fromJson(body, Devolucao.class);
		Devolucao devolucaoNovo = null;
		
		if (devolucao.getIdBicicleta() != null) {
			devolucaoNovo = new Devolucao(devolucao.getIdTranca(), devolucao.getIdBicicleta());
		}


		if (devolucaoNovo != null) {
			ctx.status(200);
			devolucoes.add(devolucaoNovo);
			ctx.result("200 - Dados cadastrados");
			alterarEstadoTranca("TRANCAR");
			incluirNaFilaCobranca();
			ControllerAluguel.enviaEmail();
		} else {
			ctx.status(422);
			ctx.result("422 - Dados inválidos");
		}
	}
	
	public static void alterarEstadoTranca(String acao) {
		Tranca nova = new Tranca(1, "rj", UUID.randomUUID().toString(), "2011", "a2", "disponível");
		String idTranca = UUID.randomUUID().toString();
		HttpResponse<JsonNode> response = Unirest.post("https://pm-2021-2.herokuapp.com/tranca/" + idTranca + "/status/" + acao).body(nova).asJson();
		
		if (response.getStatus() == 200) {
			log.debug("Operação bem sucedida");
		} else {
			log.debug("Dados inválidos");
		}
	}

	public static class Tranca {
		int numero;
		String bicicleta;
		String localizacao;
		String anoDeFabricacao;
		String modelo;
		String status;
		
		Tranca() {}
		
		public Tranca(int numero, String bicicleta, String localizacao, String anoDeFabricacao, String modelo, String status) {
			this.numero = numero;
			this.bicicleta = bicicleta;
			this.localizacao = localizacao;
			this.anoDeFabricacao = anoDeFabricacao;
			this.modelo = modelo;
			this.status = status;
		}
	}
	
	public static void incluirNaFilaCobranca() {
		String idCiclista = ControllerCiclista.ciclistas.get(0).toString();
//		Cobranca nova = new Cobranca(5.0f, idCiclista);
		HttpResponse<JsonNode> response = Unirest.post("https://pmg4.herokuapp.com/filaCobranca/").body(new Cobranca(5.0f, idCiclista))
				.asJson();
		if (response.getStatus() == 200) {
			log.debug("Cobrança feita com sucesso");
		} else {
			log.debug("Erro ao fazer a cobrança");
		}
	}
	
	
	public static Devolucao mockGetDevolucao422() {
		devolucoes.clear();
		String tranca = UUID.randomUUID().toString();
		Devolucao devolucao = new Devolucao("id", null);
		devolucoes.add(devolucao);

		return devolucoes.get(devolucoes.size() - 1);

	}

}
