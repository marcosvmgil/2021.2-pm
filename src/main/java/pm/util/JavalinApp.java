package pm.util;

import io.javalin.Javalin;
import pm.controllers.*;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
	private static final String funcionarioId = "/funcionario/:id";
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
	            		path("/ciclista", () -> post(ControllerCiclista::postCiclista));
	            		path("/ciclista/:id", () -> get(ControllerCiclista::getCiclistaById));
	            		path("/ciclista/:id", () -> put(ControllerCiclista::putCiclistaById));
	            		path("/ciclista/:id/ativar", () -> post(ControllerCiclista::postCiclistaAtivar));
	            		path("/ciclista/existeEmail/:email", () -> get(ControllerCiclista::getCiclistaEmail));
	            		path("/funcionario", () -> get(ControllerFuncionario::getFuncionario));
	            		path("/funcionario", () -> post(ControllerFuncionario::postFuncionario));
	            		path(funcionarioId, () -> get(ControllerFuncionario::getFuncionarioById));
	            		path(funcionarioId, () -> put(ControllerFuncionario::putFuncionarioById));
	            		path(funcionarioId, () -> delete(ControllerFuncionario::deleteFuncionarioById));
	            		path("/cartaoDeCredito/:id", () -> get(ControllerCartaoDeCredito::getCartaoById));
	            		path("/cartaoDeCredito/:id", () -> put(ControllerCartaoDeCredito::putCartaoById));
	            		path("/aluguel", () -> post(ControllerAluguel::postAluguel));
	            		path("/devolucao", () -> post(ControllerDevolucao::postDevolucao));
                    });
                    


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
